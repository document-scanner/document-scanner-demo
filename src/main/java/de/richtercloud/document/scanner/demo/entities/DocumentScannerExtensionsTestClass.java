/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.demo.entities;

import de.richtercloud.document.scanner.components.annotations.CommunicationTree;
import de.richtercloud.document.scanner.components.annotations.OCRResult;
import de.richtercloud.document.scanner.components.annotations.ScanResult;
import de.richtercloud.document.scanner.components.annotations.Tags;
import de.richtercloud.document.scanner.ifaces.ImageWrapper;
import de.richtercloud.document.scanner.model.WorkflowItem;
import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author richter
 */
@Entity
@SuppressWarnings("PMD.ImmutableField")
public class DocumentScannerExtensionsTestClass implements Serializable {
    private static final long serialVersionUID = 1L;
    @OCRResult
    @SuppressWarnings("PMD.UnusedPrivateField")
    private String a;
    @ScanResult
    @SuppressWarnings("PMD.UnusedPrivateField")
    private List<ImageWrapper> b;
    @CommunicationTree
    @SuppressWarnings("PMD.UnusedPrivateField")
    private List<WorkflowItem> c = new LinkedList<>();
    @Tags
    @SuppressWarnings("PMD.UnusedPrivateField")
    private Set<String> d = new HashSet<>();
    @Id
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
